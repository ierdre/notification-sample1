//
//  ViewController.swift
//  testNotifcationPermission
//
//  Created by Thibaut LE LEVIER on 12/06/2019.
//  Copyright © 2019 Cogelec. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI
class ViewController: UIViewController,UNUserNotificationCenterDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        requestPermissionsWithCompletionHandler(completion: nil)
    }
    
    //deman,de permissions notificaiton
    private func requestPermissionsWithCompletionHandler(completion: ((Bool) -> (Void))? ) {
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert]) {[weak self] (granted, error) in
            
            guard error == nil else {
                
                completion?(false)
                return
            }
            
            if granted {
                
                UNUserNotificationCenter.current().delegate = self
                self?.setNotificationCategories()
                
            }
            
            completion?(granted)
        }
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert, .sound, .badge])
        print(" Notification receive " )
    }
    @IBAction func notification1(_ sender: Any) {
       
        
        let content = UNMutableNotificationContent()
        content.title = "Local Notifications"
        content.subtitle =  "Good Morning"
        
       
        content.body = " TEST "
      
        
        content.categoryIdentifier = "local"
        
        let url = Bundle.main.url(forResource: "img1", withExtension: "png")
        
        let attachment = try! UNNotificationAttachment(identifier: "image", url: url!, options: [:])
        content.attachments = [attachment]
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 6, repeats: false)
        
        let request = UNNotificationRequest(identifier: "localNotification", content: content, trigger: trigger)
        
            UNUserNotificationCenter.current().add(request) { (error) in
                print(error)
                print(" OK3 " )
                
            
                }
        
        
    }
    
    private func setNotificationCategories() {
        
        let startAction = UNNotificationAction(identifier: "start", title: "start", options: [])
       
       
        
        let localCat =  UNNotificationCategory(identifier: "category", actions: [startAction], intentIdentifiers: [], options: [])
        

        
        UNUserNotificationCenter.current().setNotificationCategories([localCat])
        
    }
    
    
    
    
    
 

}

